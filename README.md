# MUSIC DNA and the ART of shades

Here you will learn how the beat created an excellent art. I was looking for [flight delays compensation](http://flightclaimcompensation.co.uk) and hit the studio of [aerotwist](http://lab.aerotwist.com/canvas/music-dna/). Guess what I did? I made a true color of circumference out of the Web Audio API and HTML Canvas that runs the audio through an FFT. See the result below. 

I used the song by the rock band “Blink-182 – What’s my age again?” by the way. :P

![image](http://lab.aerotwist.com/canvas/music-dna/soundna.png)